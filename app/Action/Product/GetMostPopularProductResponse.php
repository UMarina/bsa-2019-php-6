<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductResponse
{
    private $bestRatingProduct;

    public function __construct(Product $bestRatingProduct)
    {
        $this->bestRatingProduct = $bestRatingProduct;
    }

    public function getProduct() : Product
    {
        return $this->bestRatingProduct;
    }
}