<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Repository\ProductRepositoryInterface;
use App\Entity\Product;

class GetMostPopularProductAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $allProducts = $this->productRepository->findAll();
        uasort($allProducts, function (Product $first, Product $second) {
            return $first->getRating() < $second->getRating();
        });

        return new GetMostPopularProductResponse(reset($allProducts));
    }

}