<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    
    public function execute(): GetCheapestProductsResponse
    {
        $allProducts = $this->productRepository->findAll();
        uasort($allProducts, function (Product $first, Product $second) {
            return $first->getPrice() > $second->getPrice();
        });

        $cheapestProducts = array_splice($allProducts, 0, 3);

        return new GetCheapestProductsResponse($cheapestProducts);
    }
}