<?php

namespace App\Http\Controllers\Api;

use App\Action\Product\GetMostPopularProductAction;
use App\Repository\ProductRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Http\Presenter\ProductArrayPresenter;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        $allProducts = $this->productRepository->findAll();
        $presentProducts = ProductArrayPresenter::presentCollection($allProducts);
        return response()->json($presentProducts);
    }

    public function getPopular()
    {
        $action = new GetMostPopularProductAction($this->productRepository);
        $popular = $action->execute()->getProduct();
        $presentProduct = ProductArrayPresenter::present($popular);
        return response()->json($presentProduct);
    }
}