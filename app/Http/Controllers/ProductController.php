<?php

namespace App\Http\Controllers;

use App\Action\Product\GetCheapestProductsAction;
use App\Repository\ProductRepositoryInterface;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getCheapest()
    {
        $action = new GetCheapestProductsAction($this->productRepository);
        $cheapestProducts = $action->execute()->getProducts();
        return view('cheap_products', ['products' => $cheapestProducts]);
    }

}