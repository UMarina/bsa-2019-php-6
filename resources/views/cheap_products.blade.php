<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
    <style>
        .container {
            margin: 20px auto;
            width: 50%;
        }
        .container h3{
            text-align: center;
        }
        .category-list * {transition: .4s linear;}
        .category-list {
            background: white;
            list-style-type: circle;
            list-style-position: inside;
            padding: 0 10px;
            margin: 0;
        }
        .category-list li {
            font-family: "Trebuchet MS", "Lucida Sans";
            border-bottom: 1px solid #efefef;
            padding: 10px 0;
        }
        .category-list a {
            text-decoration: none;
            color: #555;
        }
        .category-list li span {
            float: right;
            display: inline-block;
            border: 1px solid #efefef;
            padding: 0 5px;
            font-size: 13px;
            color: #999;
        }
        .category-list li:hover a {color: #c93961;}
        .category-list li:hover span {
            color: #c93961;
            border: 1px solid #c93961;
        }
        .rating {
            margin-top: -8px;
            position: absolute;
            right: 30%;
            unicode-bidi: bidi-override;
            direction: rtl;
        }
        .rating > span {
            display: inline-block;
            position: relative;
            width: 1.1em;
        }
        .rating > span:before,
        .rating > span ~ span:before {
            content: "\2605";
            position: absolute;
        }
    </style>
</head>
<body>
    <div class="container">
        <h3>List of cheap products</h3>
        <ul class="category-list">
            @if($products)
                @foreach($products as $product)
                    <div class="rating">
                        @for ($i = 1; $i < $product->rating; $i++)
                            <span></span>
                        @endfor
                    </div>
                    <li>{{ $product->name }}<span>{{ $product->price }}</span></li>
                @endforeach
            @endif
        </ul>
    </div>
</body>
</html>